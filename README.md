# Hi there!

I'm Luiz Felipe ([@olooeez](https://gitlab.com/olooeez)), a passionate developer based in Belo Horizonte, Brazil. Welcome to my GitLab profile!

## Skills

- **Programming Languages:** C, C++, JavaScript, Python, PHP, Go and Perl
- **Web Technologies:** HTML, CSS, Bootstrap and Tailwind CSS
- **Web Frameworks:** Flask, React, Laravel, Symfony and Gin
- **ML Frameworks:** PyTorch and TensorFlow
- **Others:** OpenCV, Pandas and Numpy
- **Database:** MySQL, PostgreSQL and SQLite
- **Tools & Technologies:** Git, Docker and Linux
